
const state = {
  lastObserver:null,
  loginMilestone: false,
  connection:false
}

const mutations = {
  SET_LAST_OBSERVER: (state, lastObserver) => {
    state.lastObserver = lastObserver
  },
  SET_LOGIN_MILES_STONE: (state, loginMilestone) => {
    state.loginMilestone = loginMilestone
  },
  SET_CONNECTION: (state, connection) => {
    state.connection = connection
  },
  
}

const actions = {
  // user login
  setLastObserver({ commit }, SET_LAST_OBSERVER) {
    return new Promise((resolve) => {
      commit('SET_LAST_OBSERVER', SET_LAST_OBSERVER)
      resolve()
    })
  },
  setLoginState({ commit }, loginMilestone) {
    return new Promise((resolve) => {
      commit('SET_LOGIN_MILES_STONE', loginMilestone)
      resolve()
    })
  },
  setConnectionState({ commit }, connection) {
    return new Promise((resolve) => {
      commit('SET_CONNECTION', connection)
      resolve()
    })
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
}

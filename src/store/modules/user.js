import { login, getInfo, getRoleUser } from '@/api/auth'
import { isLogged, setLogged, removeToken } from '@/utils/auth'
import router, { resetRouter } from '@/router'

const state = {
  id: null,
  user: null,
  userName: null,
  token: isLogged(),
  name: '',
  avatar: '',
  organizationName: null,
  organizationReportCode: null,
  organizationShortname: null,
  roles: [],
  userInfo: {},
  permissions: [],
  resource: [],
  roleMenuButtonList: [],
}

const mutations = {
  SET_ID: (state, id) => {
    state.id = id
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  SET_PERMISSIONS: (state, permissions) => {
    state.permissions = permissions
  },
  SET_RESOURCE: (state, resource) => {
    state.resource = resource
  },
  SET_USER_NAME: (state, userName) => {
    state.userName = userName
  },
  SET_USER_ORGANIZATION: (state, organizationName) => {
    state.organizationName = organizationName
  },
  SET_USER_ORGANIZATION_CODE: (state, organizationReportCode) => {
    state.organizationReportCode = organizationReportCode
  },
  SET_USER_ORGANIZATION_SHORT_NAME: (state, organizationSortName) => {
    state.organizationShortname = organizationSortName
  },
  SET_ROLE_MENU_BUTTON_LIST: (state, roleMenuButtonList) => {
    state.roleMenuButtonList = roleMenuButtonList
  },
  SET_USER_INFO: (state, userInfo) => {
    state.userInfo = userInfo
  },
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password, systemType } = userInfo
    return new Promise((resolve, reject) => {
      login({ username: username.trim(), password: password, systemType: systemType })
        .then(response => {
          const { data } = response
          commit('SET_TOKEN', data.accessToken)
          setLogged(data.accessToken)
          resolve()
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  // get user info
  getInfo({ commit }) {
    return new Promise((resolve, reject) => {
      getInfo()
        .then(response => {
          const { data } = response
          if (!data) {
            reject('Verification failed, please Login again.')
          }
          const { fullName, avatar, uuid, userName, organizationName, organizationCode, organizationSortName } = data
          // const roles = ['admin']
          // data.roles = roles
          // // roles must be a non-empty array
          // if (!roles || roles.length <= 0) {
          //   reject('getInfo: roles must be a non-null array!');
          // }
          // commit('SET_ROLES', roles);
          // commit('SET_PERMISSIONS', permissions);
          commit('SET_NAME', fullName)
          commit('SET_USER_INFO', data)
          commit('SET_AVATAR', avatar)
          commit('SET_ID', uuid)
          commit('SET_USER_NAME', userName)
          commit('SET_USER_ORGANIZATION', organizationName)
          commit('SET_USER_ORGANIZATION_SHORT_NAME', organizationSortName)
          commit('SET_USER_ORGANIZATION_CODE', organizationCode)
          resolve(data)
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  /*getInfo({ commit }) {
    return new Promise((resolve) => {
      // const roles = state.roles;
      // const permissions = state.permissions;
      const roles = ['ROLE_ADMIN'];
      const permissions = [];

      commit('SET_ID', 'dd609a4d-3aff-4eef-a53c-9fa76d669a03');
      commit('SET_NAME', 'admin');
      commit('SET_AVATAR', '');
      commit('SET_ROLES', roles);
      commit('SET_PERMISSIONS', permissions);
      resolve({
        roles: roles,
        permissions: permissions
      });
    });
  },*/

  //get role user
  getRoleUser({ commit }) {
    return new Promise((resolve, reject) => {
      getRoleUser(state.id).then(response => {
        const { data } = response
        let roles = []
        let permissions = []
        let resource = []
        let roleMenuButtonList = []
        if (data.roleCode) {
          roles.push(data.roleCode.roleCode)
          resource = data.roleCode.roleResourcesPermissionSet
          roleMenuButtonList = data.roleCode.roleMenuButtonList
        }
        // roles must be a non-empty array
        for (let x = 0; x < data.roleCode.roleMenuSet.length; x++) {
          permissions.push(data.roleCode.roleMenuSet[x].menuId.url)
        }
        if (!roles || roles.length <= 0) {
          reject('getInfo: roles must be a non-null array!')
        }
        commit('SET_ROLES', roles)
        commit('SET_RESOURCE', resource)
        commit('SET_ROLE_MENU_BUTTON_LIST', roleMenuButtonList)
        resolve({ roles, permissions })
      })
    })
  },

  // user logout
  logout({ commit }) {
    return new Promise((resolve) => {
      const intervalId = window.localStorage.getItem("intervalId");
      if (intervalId && intervalId != '') {
        clearInterval(intervalId)
        window.localStorage.clear("intervalId")
      }
      commit('SET_TOKEN', '')
      commit('SET_ROLES', [])
      removeToken()
      resetRouter()
      resolve()
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      commit('SET_ROLES', [])
      removeToken()
      resolve()
    })
  },

  // Dynamically modify permissions
  changeRoles({ commit, dispatch }, role) {
    return new Promise(resolve => {
      // const token = role + '-token';

      // commit('SET_TOKEN', token);
      // setLogged(token);

      // const { roles } = await dispatch('getInfo');

      const roles = [role.name]
      const permissions = role.permissions.map(permission => permission.name)
      commit('SET_ROLES', roles)
      commit('SET_PERMISSIONS', permissions)
      resetRouter()
      // generate accessible routes map based on roles
      const accessRoutes = dispatch('permission/generateRoutes', { roles, permissions })
      // dynamically add accessible routes
      router.addRoutes(accessRoutes)
      resolve()
    })
  },

  // Update avatar
  updateAvatar({ commit }, avatar) {
    return new Promise((resolve) => {
      commit('SET_AVATAR', avatar)
      resolve()
    })
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
}

import request from '@/utils/request';

export function getVehicle(plate) {
  return request({
    url: `/systemconfig/vehicle-track/plate?plate=${plate}`,
    method: 'GET'
  });
}

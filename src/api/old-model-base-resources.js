import request from '@/utils/request-old';

export function oldGetData(servicePath) {
  return request({
    url: servicePath,
    method: 'GET'
  });
}

export function oldSaveData(servicePath, methodType, data) {
  return request({
    url: servicePath,
    method: methodType,
    data: data
  });
}

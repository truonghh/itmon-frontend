import request from '@/utils/request';

export function getCategoryData(servicePath) {
  return request({
    url: servicePath,
    method: 'GET'
  });
}
export function getCategoryType(servicePath) {
  return request({
    url: servicePath,
    method: 'GET'
  });
}

export function saveData(servicePath, methodType, data) {
  return request({
    url: servicePath,
    method: methodType,
    data: data
  });
}

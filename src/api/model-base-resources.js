import request from '@/utils/request';

export function getData(servicePath) {
  return request({
    url: servicePath,
    method: 'GET'
  });
}

export function saveData(servicePath, methodType, data) {
  return request({
    url: servicePath,
    method: methodType,
    data: data
  });
}
export function destroy(id) {
  return request({
    url: '/' + this.uri + '/' + id,
    method: 'delete',
  });
}


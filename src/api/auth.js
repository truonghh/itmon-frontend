import request from '@/utils/request';

export function login(data) {
  return request({
    url: '/user/login',
    method: 'post',
    data: data,
  });
}

export function getInfo(token = "") {
  if (!token) {
    return request({
      url: '/user',
      method: 'get'
    });
  } else {
    return request({
      url: '/user',
      method: 'get',
      headers: {
        Authorization: 'Bearer ' + token
      }
    });
  }
}

export function getRoleUser(id) {
  return request({
    url: '/rbac/role/user/' + id,
    method: 'get'
  })
}

export function logout(token) {
  return request({
    url: '/user/logout',
    method: 'post',
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
}

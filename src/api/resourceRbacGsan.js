import request from '@/utils/request-rbac-gsan';

/**
 * Simple RESTful resource class
 */
class Resource {
  constructor(uri) {
    this.uri = uri;
  }
  list(query) {
    return request({
      url: '/' + this.uri,
      method: 'get',
      params: query,
    });
  }
  get(id) {
    return request({
      url: '/' + this.uri + '/' + id,
      method: 'get',
    });
  }
  store(resource) {
    return request({
      url: '/' + this.uri,
      method: 'post',
      data: resource,
    });
  }

  storeParam(resource) {
    return request({
      url: '/' + this.uri,
      method: 'post',
      params: resource,
    });
  }
  update(id, resource) {
    return request({
      url: '/' + this.uri + '/' + id,
      method: 'put',
      data: resource,
    });
  }
  updateOnlyUrl(resource) {
    return request({
      url: '/' + this.uri,
      method: 'put',
      data: resource,
    });
  }
  destroy(id) {
    return request({
      url: '/' + this.uri + '/' + id,
      method: 'delete',
    });
  }
  destroyMulti(resource) {
    return request({
      url: '/' + this.uri,
      method: 'delete',
      data: resource,
    });
  }

  destroyMultiPut(resource) {
    return request({
      url: '/' + this.uri,
      method: 'put',
      data: resource,
    });
  }
  storeCustom(id, resource) {
    return request({
      url: '/' + this.uri + '/' + id,
      method: 'post',
      data: resource,
    });
  }
  storeSpecJson(id) {
    return request({
      url: '/' + this.uri + '/' + id,
      method: 'post'
    });
  }
}

export { Resource as default };

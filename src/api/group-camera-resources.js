import request from '@/utils/request';

export function getData(servicePath) {
  return request({
    url: servicePath,
    method: 'GET'
  });
}
export function getGroupCameraPaginated(query) {
  return request({
    url: '/systemconfig/group-camera',
    method: 'GET',
    params: query
  });
}
export function saveData(servicePath, methodType, data) {
  return request({
    url: servicePath,
    method: methodType,
    data: data
  });
}

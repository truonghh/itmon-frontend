import request from '@/utils/request';

export function getData(servicePath) {
  return request({
    url: servicePath,
    method: 'GET'
  });
}

export function getUsersPaginated(query) {
  return request({
    url: '/user/cms',
    method: 'GET',
    params: query
  });
}

export function saveData(servicePath, methodType, data) {
  return request({
    url: servicePath,
    method: methodType,
    data: data
  });
}

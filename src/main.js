// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Cookies from 'js-cookie'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import App from './App'
import store from './store'
import router from './router'
import Event from './mixins/event'
import checkRole from './mixins/checkRole'
import commonFT from './mixins/common'
import i18n from './lang' // Internationalization
import '@/icons' // icon
import '@/permission' // permission control
import * as filters from './filters' // global filters
import './styles/index.scss' // global css
import _ from 'lodash'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import 'fix-date'

// import * as VueGoogleMaps from "vue2-google-maps";
import VueVideoPlayer from 'vue-video-player'
import 'video.js/dist/video-js.css'
import Viewer from 'v-viewer'
import 'viewerjs/dist/viewer.css'
import 'videojs-youtube'
import ImageHorver from '@/components/ImageHover/index'
import VueHtmlToPaper from 'vue-html-to-paper';
import VueZoomer from 'vue-zoomer'
// import VueSocketIOExt from 'vue-socket.io-extended'
// import SocketIO from 'socket.io-client'
import VueSocketIO from 'vue-socket.io'
import SocketIO from 'socket.io-client'

// import default_img_cam from '@/assets/images/default_img_cam.png';
// import 'vue-video-player/src/custom-theme.css'

// import VueSocketIO from 'vue-socket.io'
// import VueSocketIOExt from 'vue-socket.io-extended'
// import SocketIO from 'socket.io-client'
import hls from 'videojs-contrib-hls'

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('image-hover', ImageHorver)

// window.config = require('./config.js');

Vue.config.productionTip = false

export const bus = new Vue();
Vue.use(hls)
Vue.config.devtools = true;
Vue.use(ElementUI, {
  size: Cookies.get('size') || 'medium', // set element-ui default size
  i18n: (key, value) => i18n.t(key, value),
})
window.moment = require('moment');
Vue.use(require('vue-moment'))

// Vue.use(VueGoogleMaps, {
//     load: {
//         // key: 'AIzaSyAS0lND3EMdFR5345345CtdjDfGxKigLVJJnI',
//         // libraries: []
//     }
// })

let options = {
  withCredentials: false,
  autoConnect: false,
  query: {
    token: ''
  }
}

let ioParams = {'reconnection limit': 3000, 'max reconnection attempts': Number.MAX_VALUE, 'connect timeout':3000};
Vue.use(new VueSocketIO({
    debug: false,
    connection: SocketIO(process.env.VUE_APP_BASE_SOCKET, options, ioParams), //options object is Optional
    vuex: {
      actionPrefix: "SOCKET_",
      mutationPrefix: "SOCKET_"
    }
  })
)

Vue.use(Viewer)

Vue.use(VueHtmlToPaper)
Vue.use(VueVideoPlayer,  {
  // options: global default options,
  // events: global videojs events
})
Vue.use(VueZoomer)

// register global utility filters.
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  el: '#app',
  mixins: [Event, checkRole,commonFT],
  router,
  store,
  i18n,
  components: { App },
  template: '<App/>',
  created() {
    // console.log(this.$socket);
  }
  // sockets: {
  //   connect: function () {
  //       console.log('socket connected')
  //   }
  // },
})

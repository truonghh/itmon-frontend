import moment from 'moment';

const APP_LOCAL_DATETIME_FORMAT_Z = 'YYYY-MM-DDTHH:mm Z';
const APP_TIMESTAMP_FORMAT = 'DD/MM/YY HH:mm:ss';

export const convertDateTimeFromServer = date => (date ? moment(date).format(APP_TIMESTAMP_FORMAT) : null);

export const convertDateTimeToServer = date => (date ? moment(date, APP_LOCAL_DATETIME_FORMAT_Z).toDate() : null);

export function formatDate(date) {
  if (date) {
    return moment(date).format('DD/MM/YY HH:mm:ss');
  } else {
    return '--';
  }
}

export function formatDateHHMM(date) {
  if (date) {
    return moment(date).format('DD/MM/YYYY HH:mm');
  } else {
    return '--';
  }
}

export function formatDateTSHHMM(date) {
  if (date) {
    return moment.unix(date).format('DD/MM/YYYY HH:mm');
  } else {
    return '--';
  }
}
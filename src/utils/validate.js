/* All validations should be defined here */

export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path);
}

/**
 * Validate a valid URL
 * @param {String} textval
 * @return {Boolean}
 */
export function validURL(url) {
  const reg = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
  return reg.test(url);
}

/**
 * Validate a full-lowercase string
 * @return {Boolean}
 * @param {String} str
 */
export function validLowerCase(str) {
  const reg = /^[a-z]+$/;
  return reg.test(str);
}

/**
 * Validate a full-uppercase string
 * @return {Boolean}
 * @param {String} str
 */
export function validUpperCase(str) {
  const reg = /^[A-Z]+$/;
  return reg.test(str);
}

/**
 * Check if a string contains only alphabet
 * @param {String} str
 * @param {Boolean}
 */
export function validAlphabets(str) {
  const reg = /^[A-Za-z]+$/;
  return reg.test(str);
}

/**
 * Validate an email address
 * @param {String} email
 * @return {Boolean}
 */
export function validEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

/**
 * Validate mobile number
 * @param {String} phone
 * @return {Boolean}
 */
export function validPhone(phone){
  phone = phone.replace(/[^0-9]/g, "")
  // Viettel: 09, 03
  // MobiFone: 09, 07
  // VinaPhone: 09, 08
  // Vietnamobile và Gmobile: 09, 05
  // var vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
  var vnf_regex = /((0)+([0-9]{9,10})\b)/g;
  return vnf_regex.test(phone);
}

/**
 * Validate float
 * @param {String} str
 * @param {Boolean}
 */
 export function validFloat(str) {
  const reg = /^([0-9]*[.])?[0-9]+$/;
  return reg.test(str);
}

/**
 * Validate longitude and latitude
 * @param {String} str
 * @param {Boolean}
 */
 export function validateLonLat(str) {
  // const reg = /^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}+$/;
  // return reg.test(str);

  let pattern = new RegExp('^-?([1-8]?[1-9]|[1-9]0)\\.{1}\\d{1,6}');
  return pattern.test(str);
}

/**
 * Check if a string contains alphabet, number
 * @param {String} str
 * @param {Boolean}
 */
 export function validAlphabetsNumber(str) {
  // const reg = /^[A-Za-z0-9][!@#$%^&*(),.?":{}|<>]+$/;
  const reg = /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+$/g;
  return reg.test(str);
}

export function isLatitude(lat) {
  return isFinite(lat) && Math.abs(lat) <= 90;
}

export function isLongitude(lng) {
  return isFinite(lng) && Math.abs(lng) <= 180;
}

/**
 * Validate plate
 * @param {String} str
 * @param {Boolean}
 */
export function validPlate(str) {
  if(!str) {
    return true
  }
  // const reg = /^[0-9]{2}[A-Z]{1,2}[0-9]{5,6}$/;
  const reg = /^[0-9a-zA-ZĐ]{3,10}$/;
  return reg.test(str);
}

export function validIdcard(str) {
  if(!str) {
    return true
  }
  // const reg = /^[0-9]{2}[A-Z]{1,2}[0-9]{5,6}$/;
  const reg = /^[0-9]{9}$|^[0-9]{12}$/;
  return reg.test(str);
}

export function validPort(str) {
  str = str.trim()
  if(!str) {
    return true
  }
  // const reg = /^[0-9]{2}[A-Z]{1,2}[0-9]{5,6}$/;
  const reg = /^[0-9]{2,4}$/;
  return reg.test(str);
}

export function validIp(str) {
  str = str.trim()
  if(!str) {
    return true
  }
  // const reg = /^[0-9]{2}[A-Z]{1,2}[0-9]{5,6}$/;
  const reg = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
  return reg.test(str);
}


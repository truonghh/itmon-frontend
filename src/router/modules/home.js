/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const routes = {
  path: '/',
  component: Layout,
  hidden: true,
  children: [
    {
      path: '',
      component: () => import('@/views/home/index'),
      name: 'Home',
      meta: { title: '', icon: 'piechart', noCache: false},
    },
  ],
};

export default routes;

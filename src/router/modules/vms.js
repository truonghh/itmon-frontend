/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const routes = {
  path: '',
  component: Layout,
  meta: {
    permissions: ['/vms']
  },
  children: [
    {
      path: 'vms',
      name: 'vms',
      component: () => import('@/views/vms/index'),
      meta: { title: 'Quản lý VMS', icon: 'xmvp', noCache: true, permissions: ['/vms'] },
    },
    {
      path: 'camera-for-vms/:id',
      component: () => import('@/views/vms/camera'),
      hidden: true,
      name: 'cameraForVms',
      meta: { title: 'Quản lý VMS', icon: 'cam1', noCache: false},
    },
  ],
};

export default routes;

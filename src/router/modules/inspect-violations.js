/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const routes = {
  path: '/verify-violation',
  component: Layout,
  redirect: 'noredirect',
  hidden: false,
  meta: {
    title: 'Kiểm duyệt vi phạm',
    icon: 'verify-violation',
    permissions: ['/verify-violation']
  },
  children: [
    {
      path: 'index',
      name: 'inspect-violations',
      component: () => import('@/views/inspect-violations/index'),
      meta: { title: 'Kiểm duyệt vi phạm', noCache: true, permissions: ['/index'] },
    },
    {
      path: 'violation/inspect-violations-detail',
      name: 'detail',
      component: () => import('@/views/inspect-violations/inspect-violations-detail'),
      hidden: true,
      meta: { title: 'Kiểm duyệt vi phạm', noCache: true }
    },
    {
      path: 'violation/inspect-violations/inspect',
      name: 'violationInspect',
      component: () => import('@/views/inspect-violations/inspect'),
      hidden: true,
      meta: { title: 'Kiểm duyệt vi phạm', noCache: true }
    },
    {
      path: 'noviolation',
      name: 'noviolation',
      component: () => import('@/views/inspect-violations/noviolation'),
      hidden: false,
      meta: { title: 'Không vi phạm', noCache: true, permissions: ['/noviolation'] }
    },
    {
      path: 'noviolation/detail',
      name: 'noviolation-detail',
      component: () => import('@/views/inspect-violations/noviolation-detail'),
      hidden: true,
      meta: { title: 'Không vi phạm', noCache: true }
    },
    // {
    //   path: 'inspect-violations/inspect-tmp',
    //   name: 'violationInspectTmp',
    //   component: () => import('@/views/inspect-violations/inspect-tmp'),
    //   hidden: false,
    //   meta: { title: 'Test biển số', noCache: true }
    // }
  ],
};

export default routes;

/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const searchRoutes = {
  path: '/search',
  component: Layout,
  redirect: 'noredirect',
  hidden: false,
  meta: {
    title: 'Quản lý phương tiện',
    icon: 'vehicle-monitor',
    permissions: ['/search'],
  },
  children: [
    // {
    //   path: 'violation',
    //   component: () => import('@/views/search/violation'),
    //   meta: { title: 'Tìm kiếm vi phạm', noCache: true, permissions: ['/violation'] },
    // },
    {
      path: 'vehicle-track',
      component: () => import('@/views/system-config/vehicle-track/index'),
      meta: { title: 'Quản lý đối tượng', noCache: true, permissions: ['/vehicle-track'] }
    },
    {
      path: 'recognition',
      component: () => import('@/views/search/recognition'),
      meta: { title: 'Tra cứu phương tiện', noCache: true, permissions: ['/recognition'] },
    }
  ],
};

export default searchRoutes;

/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const routes = {
  path: '/report',
  component: Layout,
  redirect: 'noredirect',
  hidden: false,
  meta: {
    title: 'Báo cáo thống kê',
    icon: 'file-text',
    permissions: ['/report']
  },
  children: [
    {
      path: 'no-process-detail',
      name: 'no-process-detail',
      hidden: true,
      component: () => import('@/views/report/noprocess-detail'),
      meta: { title: 'Chi tiết vp chưa xử lý', noCache: true, permissions: ['/verify-violation'] },
    }, {
      path: 'no-process-total',
      name: 'no-process-total',
      hidden: true,
      component: () => import('@/views/report/noprocess-total'),
      meta: { title: 'Tổng hợp vp chưa xử lý', noCache: true, permissions: ['/verify-violation'] },
    },
    {
      path: 'processed-detail',
      name: 'processed-detail',
      hidden: true,
      component: () => import('@/views/report/processed-detail'),
      meta: { title: 'Chi tiết vi phạm đã xử lý', noCache: true, permissions: ['/verify-violation'] },
    },
    {
      path: 'processed-total',
      name: 'processed-total',
      hidden: true,
      component: () => import('@/views/report/processed-total'),
      meta: { title: 'Tổng hợp vp đã xử lý', noCache: true, permissions: ['/verify-violation'] },
    },
    {
      path: 'violation-total',
      name: 'violation-total',
      hidden: true,
      component: () => import('@/views/report/violation-total'),
      meta: { title: 'Tổng hợp vi phạm', noCache: true, permissions: ['/verify-violation'] },
    },
    // bao cao chart
    {
      path: 'traffic-flow-vehicles',
      name: 'traffic-flow-vehicles',
      component: () => import('@/views/report/traffic-flow-vehicle'),
      meta: { title: 'Báo cáo lưu lượng', noCache: true, permissions: ['/traffic-flow-vehicles'] },
    },
    {
      path: 'report-violation',
      name: 'ReportViolation',
      component: () => import('@/views/report/violation'),
      meta: { title: 'Báo cáo vi phạm', noCache: true, permissions: ['/report-violation'] },
    },
    {
      path: 'report-process-violation',
      name: 'ReportProcessViolation',
      component: () => import('@/views/report/violation-process'),
      meta: { title: 'Báo cáo xử lý vi phạm', noCache: true, permissions: ['/report-process-violation'] },
    },
    // ket thuc
    {
      path: 'violation-vehicle',
      name: 'violation-vehicle',
      hidden: true,
      component: () => import('@/views/report/violation-vehicle'),
      meta: { title: 'Vi phạm theo pt', noCache: true, permissions: ['/verify-violation'] },
    },
    {
      path: 'violation-type',
      name: 'violation-type',
      hidden: true,
      component: () => import('@/views/report/violation-type'),
      meta: { title: 'Vi phạm theo loại', noCache: true, permissions: ['/verify-violation'] },
    },
    {
      path: 'violation-process-status',
      name: 'violation-process-status',
      hidden: true,
      component: () => import('@/views/report/violation-process-status'),
      meta: { title: 'Vi phạm theo trang thái', noCache: true, permissions: ['/verify-violation'] },
    }
  ],
};

export default routes;

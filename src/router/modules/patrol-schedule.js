/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const searchRoutes = {
  path: '/patrol-schedule',
  component: Layout,
  hidden: false,
  meta: {
    title: 'Lịch tuần tra',
    icon: 'patrol-schedule',
    permissions: ['/patrol-schedule'],
  },
  children: [
    {
      path: 'my-patrol-schedule',
      name: 'myPatrolSchedule',
      component: () => import('@/views/patrol-schedule/my-patrol-schedule'),
      meta: { title: 'Lịch tuần tra của tôi', noCache: true, permissions: ['/my-patrol-schedule'] },
    },
    {
      path: 'patrol-schedule-management',
      name: 'managementPatrolSchedule',
      component: () => import('@/views/patrol-schedule/patrol-schedule-management'),
      meta: { title: 'Quản lý lịch tuần tra', noCache: true, permissions: ['/patrol-schedule-management'] },
    }
  ],
};

export default searchRoutes;

/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const routes = {
  path: '',
  component: Layout,
  meta: {permissions: ['/dashboard']},
  children: [
    {
      path: 'dashboard',
      component: () => import('@/views/dashboard/index'),
      name: 'Dashboard',
      meta: { title: 'dashboard', icon: 'piechart', noCache: false, permissions: ['/dashboard']},
    },
  ],
};

export default routes;

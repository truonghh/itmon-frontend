/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const monitorRoutes = {
  path: '/monitor',
  component: Layout,
  redirect: 'noredirect',
  hidden: false,
  meta: {
    title: 'Giám sát giao thông',
    icon: 'traffic-monitor',
    permissions: ['/monitor']
  },
  children: [
    {
      path: 'map',
      component: () => import('@/views/monitor/map'),
      meta: { title: 'Bản đồ GIS', noCache: true, permissions: ['/map'] },
    },
    {
      path: 'camera',
      name: 'monitor_camera',
      component: () => import('@/views/monitor/camera'),
      meta: { title: 'Giám sát camera', noCache: true, permissions: ['/camera'] },
    },
  ],
};

export default monitorRoutes;

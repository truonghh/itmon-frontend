/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const routes = {
  path: '/notify-list',
  component: Layout,
  redirect: 'noredirect',
  hidden: true,
  meta: {
    title: 'Thông báo',
    icon: 'el-icon-document',
    permissions: ['/notify-list'],
  },
  children: [
    {
      path: '/',
      component: () => import('@/views/notify'),
      meta: { title: 'Thông báo', noCache: true, permissions: ['/notify-list'] },
    },
  ],
};

export default routes;

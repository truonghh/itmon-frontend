/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const routes = {
  path: '/system-config',
  component: Layout,
  redirect: 'noredirect',
  hidden: false,
  meta: {
    title: 'Cấu hình hệ thống',
    icon: 'cog',
    permissions: ['/system-config']
  },
  children: [
    {
      path: 'site',
      component: () => import('@/views/system-config/sites/index'),
      meta: { title: 'Quản lý vị trí', noCache: true, permissions: ['/site'] }
    },

    {
      path: 'cameras',
      component: () => import('@/views/system-config/cameras/index'),
      meta: { title: 'Quản lý camera', noCache: true, permissions: ['/cameras'] }
    },
    {
      path: 'cameras/:id/detail-layout',
      component: () => import('@/views/system-config/cameras/detail-layout'),
      name: 'CameraDetailLayout',
      hidden: true,
      meta: { title: 'Chi tiết layout camera', noCache: true },
    },
    {
      path: 'cameras/:id/layout',
      component: () => import('@/views/system-config/cameras/detail-security-layout'),
      name: 'CameraDetailSecurityLayout',
      hidden: true,
      meta: { title: 'Chi tiết layout camera', noCache: true },
    },
    {
      path: 'camera-group',
      component: () => import('@/views/system-config/group-camera/index'),
      meta: { title: 'Quản lý nhóm camera', noCache: true, permissions: ['/camera-group'] }
    },

    {
      path: 'role',
      component: () => import('@/views/system-config/role/index'),
      meta: { title: 'Quản lý quyền', noCache: true, breadcrumb: true, permissions: ['/role'] },
    },
    {
      path: 'user',
      component: () => import('@/views/system-config/user/index'),
      meta: { title: 'Quản lý người dùng', noCache: true, permissions: ['/user'] }
    },
    {
      path: 'organization',
      component: () => import('@/views/system-config/organization/index'),
      meta: { title: 'Quản lý đơn vị', noCache: true, permissions: ['/organization'] }
    },
    {
      path: 'group',
      component: () => import('@/views/system-config/group/index'),
      meta: { title: 'Quản lý đội CSGT', noCache: true, permissions: ['/group'] }
    },
    {
      path: 'role/add',
      component: () => import('@/views/system-config/role/add'),
      hidden: true,
      meta: { title: 'Thêm mới quyền', noCache: true }
    },
    {
      path: 'role/edit/:roleCode',
      component: () => import('@/views/system-config/role/edit'),
      hidden: true,
      meta: { title: 'Quản lý quyền', noCache: true }
    },
    // {
    //   path: 'vehicle-track',
    //   component: () => import('@/views/system-config/vehicle-track/index'),
    //   meta: { title: 'Danh sách theo dõi', noCache: true, permissions: ['/vehicle-track']}
    // },
    {
      path: 'category',
      component: () => import('@/views/system-config/category/index'),
      meta: { title: 'Danh sách danh mục', noCache: true, permissions: ['/category'] }
    },
    {
      path: 'process-unit',
      component: () => import('@/views/system-config/process-unit/index'),
      meta: { title: 'Quản lý khối xử lý', noCache: true, permissions: ['/process-unit'] }
    },
    {
      path: 'vms',
      name: 'vms',
      component: () => import('@/views/vms/index'),
      meta: { title: 'Quản lý VMS', noCache: true, permissions: ['/vms'] },
    },
    {
      path: 'camera-for-vms/:id',
      component: () => import('@/views/vms/camera'),
      hidden: true,
      name: 'cameraForVms',
      meta: { title: 'Quản lý VMS', icon: 'cam1', noCache: false },
    },
    {
      path: 'holiday',
      component: () => import('@/views/system-config/holiday/index'),
      meta: { title: 'Quản lý ngày lễ', noCache: true,permissions: ['/holiday'] }
    },
  ]
};

export default routes;

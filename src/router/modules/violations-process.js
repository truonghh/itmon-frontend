/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const routes = {
  path: '/process-violation',
  component: Layout,
  redirect: 'noredirect',
  hidden: false,
  meta: {
    title: 'Xử lý vi phạm',
    icon: 'xlvp',
    permissions: ['/process-violation'],
  },
  children: [
    {
      path: 'checked-process',
      component: () => import('@/views/violations-process/checked-process'),
      meta: { title: 'Vi phạm chưa xử lý', noCache: true, permissions: ['/checked-process'] },
    },
    {
      path: 'processing',
      component: () => import('@/views/violations-process/processing'),
      meta: { title: 'Vi phạm đang xử lý', noCache: true, permissions: ['/processing'] },
    },
    {
      path: 'processed',
      component: () => import('@/views/violations-process/processed'),
      meta: { title: 'Vi phạm đã xử lý', noCache: true, permissions: ['/processed'] },
    },
    {
      path: 'cold-penalty',
      component: () => import('@/views/violations-process/cold-penalty'),
      meta: { title: 'Xử lý phạt nguội', noCache: true, permissions: ['/cold-penalty'] },
    },
    {
      path: 'blacklist',
      component: () => import('@/views/violations-process/blacklist'),
      meta: { title: 'Vi phạm quá hạn', noCache: true, permissions: ['/blacklist'] },
    },
    {
      path: 'scene-violation',
      component: () => import('@/views/violations-process/scene-violation'),
      meta: { title: 'Vi phạm hiện trường', noCache: true, permissions: ['/scene-violation'] },
    },
    {
      path: 'violation',
      component: () => import('@/views/search/violation'),
      meta: { title: 'Tra cứu vi phạm', noCache: true, permissions: ['/violation'] },
    }
  ],
};

export default routes;

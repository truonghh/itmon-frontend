export default {
	methods: {
		checkRole: function (fullPath, roleDetail = 'canRead') {
			let uri = '/' + fullPath.substring(fullPath.lastIndexOf('/') + 1);
			uri = this.removeURLParam(uri, '?')
			let listButton = this.$store.state.user.roleMenuButtonList;
			let role = this.$store.state.user.roles;
			role = role.filter(function (val) {
				return val == 'XPGT_ADMIN';
			});
			if (role.length > 0) {
				return true;
			}
			if (listButton && listButton.length > 0) {
				listButton = listButton.filter(function (val) {
					return val.url == uri;
				});
			}
			let result = false;
			if (listButton && listButton.length > 0) {
				if (listButton[0].[roleDetail] == 1) {
					result = true;
				}
			}
			return result;
		},
		removeURLParam(uri) {
			return uri.split('?')[0];
		},
		checkAdmin: function (fullPath) {
			let uri = '/' + fullPath.substring(fullPath.lastIndexOf('/') + 1);
			uri = this.removeURLParam(uri, '?')
			let role = this.$store.state.user.roles;
			let result = false;
			role = role.filter(function (val) {
				result = (val == 'XPGT_ADMIN');
			});
			//console.log(result)
			return result;
		}
	}
}
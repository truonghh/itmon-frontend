export default {
    methods: {
        eventColor(event) {
            const colors = {
                wrongline: '#5ab2ed',
                redlight: '#aed760',
                restricted: '#ff5947',
                speedlimit: '#5c6ac4',
                unknown: '#ef8498',
                noentry: '#bf9dee',
                illegalstop: '#00a748',
                inversedirection: '#ffc274',
                nohelmet: '#2cdae7',
                overload: '#0319fa'
            }
            event = event.toLowerCase()
            return colors[event] ? colors[event] : '#808080'
        },
        getEventIcon(icon) {
            const icons = {
                wrongline: 'wrongline',
                redlight: 'redlight',
                restricted: 'restricted',
                speedlimit: 'speedlimit',
                unknown: 'unknown',
                noentry: 'noentry',
                illegalstop: 'illegalstop',
                inversedirection: 'inversedirection',
                nohelmet: 'nohelmet',
                overload: 'overload',
                //  For notify:
                icon9: 'redlight',
                icon14: 'wrongline',
                icon13: 'speedlimit',
                icon12: 'noentry',
                icon8: 'illegalstop',
                icon10: 'inversedirection',
                icon11: 'restricted',
                icon15: 'nohelmet',
                icon16: 'overload'
            }
            icon = icon.toLowerCase()
            return icons[icon] ? icons[icon] : 'event-default'
        }
    }
}

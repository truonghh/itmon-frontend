import store from '@/store';

export default {
	data(){
		return{
			methodsCallback:[],
		}
	},
	methods: {
		handleAddMethodCallback(methods){
			if(methods){
				this.methodsCallback.push(methods)
			}
		},
		async connectToServer() {
			// Connect to the desired server (defaults to the current URL)
			let url = process.env.VUE_APP_MILESTONE_URL;
			XPMobileSDKSettings.MobileServerURL = url;
			
			const self = this;
			// if(!loginSuccessCallBack){
			// 	loginSuccessCallBack = self.handleLoginDid
			// }
			const lastObserver = {
			  connectionDidConnect: self.connectionDidConnect,
			  connectionDidLogIn: this.handleLoginDid,
			  // connectionFailedToLogIn: self.connectionFailedToLogIn,
			};
		   
			// await store.dispatch('common/setLastObserver',lastObserver);
			XPMobileSDK.addObserver(lastObserver);
			
			XPMobileSDK.connect(url);
		  },
	  
		  async connectionDidConnect(data){
			 await this.login();
		  },
		  async login() {
			const userName = process.env.VUE_APP_MILESTONE_USERNAME;
			const pass = process.env.VUE_APP_MILESTONE_PASSWORD
			XPMobileSDK.login(userName, pass, "ActiveDirectory", {
			  SupportsAudioIn: "Yes",
			  SupportsAudioOut: "Yes",
			});
		  },
		  
		  delay(ms) {
			return new Promise((res) => setTimeout(res, ms));
		  },
		  async handleLoginDid(data){
			await store.dispatch('common/setLoginState',true);
			this.methodsCallback.forEach(element => {
				element()
			});
			this.methodsCallback = []
		  },
		trimData(data) {
	      for(let key in data){
	      	if (data[key] && typeof data[key] === 'string') {
	        	data[key] = data[key] ? data[key].trim() : null
	    	}
	      }
	      return data;
	    },
	    detectFormatVideo(link) {
	    	if (typeof link === 'string' || link instanceof String){
	    		link = link.split('.');
	    		return link[link.length-1]
	    	}
	    	return link
	    },
	    detectSizeInPageNoImage() {
	    	let size = 10
	    	let browserZoomLevel = Math.round(window.devicePixelRatio * 100)
	    	if(browserZoomLevel == 100 || browserZoomLevel > 100) {
	    		size = 15
	    	}else if(browserZoomLevel <= 100 && browserZoomLevel >= 90) {
	    		size = 18
	    	} else if(browserZoomLevel < 90 && browserZoomLevel >= 80) {
	    		size = 21
	    	} else if(browserZoomLevel < 80 && browserZoomLevel >= 70) {
	    		size = 22
	    	} else if(browserZoomLevel < 70 && browserZoomLevel >= 60) {
	    		size = 25
	    	} else if(browserZoomLevel < 60 && browserZoomLevel >= 50) {
	    		size = 35
	    	}else{
	    		size = 40
	    	}
			return size
	    },
	    detectSizeInPageHasImage() {
	    	let size = 12
	    	let browserZoomLevel = Math.round(window.devicePixelRatio * 100)
	    	if(browserZoomLevel == 100 || browserZoomLevel > 100) {
	    		size = 12
	    	}else if(browserZoomLevel < 100 && browserZoomLevel >= 90) {
	    		size = 15
	    	} else if(browserZoomLevel < 90 && browserZoomLevel >= 80) {
	    		size = 17
	    	} else if(browserZoomLevel < 80 && browserZoomLevel >= 70) {
	    		size = 18
	    	} else if(browserZoomLevel < 70 && browserZoomLevel >= 60) {
	    		size = 25
	    	} else if(browserZoomLevel < 60 && browserZoomLevel >= 50) {
	    		size = 35
	    	}else{
	    		size = 40
	    	}
			return size
	    },
	    isIpad() {
	    	const ua = window.navigator.userAgent;
		    if (ua.indexOf('iPad') > -1) {
		        return true;
		    }

		    if (ua.indexOf('Macintosh') > -1) {
		        try {
		            document.createEvent("TouchEvent");
		            return true;
		        } catch (e) {}
		    }
		    return false;
	    }
	}
}
module.exports = {
  areas_roi_types: {
    NGUOC_CHIEU: 1,
    DUNG_DO_TRAI_PHEP: 4,
    DO_DEM_LUU_LUONG: 5,
    TRANG_THAI_DEN_GIAO_THONG: 7,
    VI_PHAM_DEN_TIN_HIEU: 8,
    NHAN_DIEN_BIEN_SO: 9,
    DUONG_CAM: 18,
    HUONG_DI_CHUYEN: 16,
    SAI_CHI_DAN_VACH_KE_DUONG: 17,
    SAI_LAN: 18,
    XAM_NHAP_VUNG_CAM: 19,
    PHAT_HIEN_SU_CO: 31,
    DAM_DONG: 90,
    BAO_CHAY_BAO_KHOI: 91,
    DUNG_DO_LAN_CHIEM: 92,
    XA_RAC: 93,
    NGAP_LUT: 94,
  },


  timeVideoSave: 60,

  resolutionList: ["1024x768", "1024x576", "1280x960", "1280x720"],

  listSizeImage: [
    {
      id: '1024_768',
      name: '1024x768 (4:3)'
    },
    {
      id: '1024_576',
      name: '1024x576 (16:9)'
    },
    {
      id: '1280_960',
      name: '1280x960 (4:3)'
    },
    {
      id: '1280_720',
      name: '1280x720 (16:9)'
    },
  ],
  eventState: [
    {
      id: '',
      name: 'Tất cả'
    },
    {
      id: 'CONFIRMED',
      name: 'Chắc chắn'
    },
    {
      id: 'IDLE',
      name: 'Nghi ngờ'
    }
  ],
  recognitionStatus: [
    {
      id: 'ĐB',
      name: 'Nhận dạng đúng'
    },
    {
      id: 'KĐ',
      name: 'Mắt không nhìn được'
    },
    {
      id: 'SB',
      name: 'Sai biển'
    }
  ],
  violationProcessStatus : {
    WAIT: "Chờ kiểm duyệt",
    IDLE: "Chờ kiểm duyệt lại",
    CHECKED: "Đã kiểm duyệt",
    UNKNOWN: "Không xác định",
    PROCESSED: "Đã xử lý",
    COLD_PENALTY: "Thông báo lần 1",
    VIOLATE: "Vi phạm",
    NOVIOLATE: "Không vi phạm",
    COLD_PENALTY_OUT_1: "Quá hạn lần 1",
    COLD_PENALTY_OUT_2 : "Thông báo lần 2",
    BLACKLIST: "Vi phạm quá hạn",
    MINUTES_PENALTY: "Đã lập BBXP",
    DECISION_PENALTY: "Đã lập QĐXP",
    DECISION_PENALTY_NO_MINUTES: "Đã lập QĐXP không qua biên bản"
  },

  // location default for map
  map_location_default: {
    longitude: 105.776665,
    latitude: 19.807627
  },

  // color
  colorList: [
    { code: 'Red', name: 'Đỏ' },
    { code: 'Blue', name: 'Xanh biển' },
    { code: 'Green', name: 'Xanh lá' },
    { code: 'Black', name: 'Đen' },
    { code: 'White', name: 'Trắng' },
    { code: 'Gray', name: 'Xám' },
    { code: 'Brown', name: 'Nâu' },
    { code: 'Yellow', name: 'Vàng' },
    { code: 'Orange', name: 'Cam' },
    { code: 'Silver', name: 'Bạc' },
  ],

  // brand
  brandList: [
      { code: 'Audi', name: 'Audi' },
      { code: 'BMW', name: 'BMW' },
      { code: 'Chervrolet', name: 'Chervrolet' },
      { code: 'Daewoo', name: 'Daewoo' },
      { code: 'Ford', name: 'Ford' },
      { code: 'Ford Ranger', name: 'Ford Ranger' },
      { code: 'Ford Transit', name: 'Ford Transit' },
      { code: 'Honda', name: 'Honda' },
      { code: 'Huyndai', name: 'Huyndai' },
      { code: 'Isuzu', name: 'Isuzu' },
      { code: 'Kia', name: 'Kia' },
      { code: 'Kia Morning', name: 'Kia Morning' },
      { code: 'Kia Pride', name: 'Kia Pride' },
      { code: 'Lexus', name: 'Lexus' },
      { code: 'Mazda', name: 'Mazda' },
      { code: 'Mercedes', name: 'Mercedes' },
      { code: 'Minicooper', name: 'Minicooper' },
      { code: 'Mitsubishi', name: 'Mitsubishi' },
      { code: 'Nissan', name: 'Nissan' },
      { code: 'Other', name: 'Other' },
      { code: 'Peugeot', name: 'Peugeot' },
      { code: 'Porsche', name: 'Porsche' },
      { code: 'Rangerover', name: 'Rangerover' },
      { code: 'Renault', name: 'Renault' },
      { code: 'Subaru', name: 'Subaru' },
      { code: 'Suzuki', name: 'Suzuki' },
      { code: 'Samco', name: 'Samco' },
      { code: 'Toyota', name: 'Toyota' },
      { code: 'Toyota Hilux', name: 'Toyota Hilux' },
      { code: 'Toyota Zace', name: 'Toyota Zace' },
      { code: 'Volkswagen', name: 'Volkswagen' },
  ],
};
